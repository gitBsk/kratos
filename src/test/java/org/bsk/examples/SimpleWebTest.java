package org.bsk.examples;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SimpleWebTest {

    public static Logger log;
    public WebDriver driver;

    @BeforeClass
    public void initializeChromeDriverAndLogger() {

        //Creating Chrome Driver to run the tests
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/java/drivers/chromedriver");
        driver = new ChromeDriver();

        //Logger
        log = LogManager.getLogger(SimpleWebTest.class.getName());

    }

    @Test
    public void getAltimetrikHomePage() {

        log.debug("Test Started");
        driver.get("https://www.altimetrik.com/");
        Assert.assertEquals(driver.getTitle(), "Home - Altimetrik Digital Transformation Catalyst | Home");
        log.info("AM Website Loaded and Title Asserted");
    }

    @AfterMethod
    public void closeChromeWindow() {
        driver.close();
    }

}
